import 'package:bengkel_online/bloc/auth/auth_bloc.dart';
import 'package:bengkel_online/module/etc/color.dart';
import 'package:bengkel_online/module/etc/navigate.dart';
import 'package:bengkel_online/view/auth/login.dart';
import 'package:bengkel_online/view/home/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => AuthBloc()
            ..add(
              CheckSession(),
            ),
        ),
      ],
      child: MaterialApp(
        title: 'Bengkel Online',
        navigatorKey: navKey,
        theme: ThemeData(scaffoldBackgroundColor: bgColor),
        home: Login(),
      ),
    );
  }
}
