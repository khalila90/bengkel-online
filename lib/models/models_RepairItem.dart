import 'package:bengkel_online/models/menu_item_models.dart';

class ModelRepairItem {
  String name;
  String type;
  int age;
  String distance;

  ModelRepairItem(
    this.name,
    this.type,
    this.age,
    this.distance,
  );
}

List<ModelRepairItem> getRepairModel() {
  return <ModelRepairItem>[
    ModelRepairItem(
      "Bagus Effendi",
      "Motorcycle Repair",
      39,
      "2 KM",
    ),
    ModelRepairItem(
      "Heri Prasetyo",
      "Printer Repair",
      19,
      "5 KM",
    ),
    ModelRepairItem(
      "Rifki Cahyo Prayoga",
      "Car Repair",
      24,
      "7 KM",
    ),
  ];
}
