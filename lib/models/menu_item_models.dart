import 'package:flutter/cupertino.dart';

class MenuItem {
  String title;
  String imgPath;
  String code;

  MenuItem(this.title, this.imgPath, this.code);
}

List<MenuItem> getItemMenu() {
  return <MenuItem>[
    MenuItem("Motorcycle Repair", "assets/icons/motor_repair.png", "mtr"),
    MenuItem("Car Repair", "assets/icons/car_repair.png", "car"),
    MenuItem("Tire Repair", "assets/icons/tire_repair.png", "tire"),
    MenuItem("Printer Repair", "assets/icons/printer_repair.png", "ptr"),
    MenuItem("Laptop/Pc Repair", "assets/icons/laptop_repair.png", "lp"),
  ];
}
