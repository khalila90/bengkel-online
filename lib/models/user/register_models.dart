class RegisterUserModel {
  String? typeAccount;
  String? name;
  String? phone;
  String? email;
  String? password;
  RegisterUserModel(
    this.typeAccount,
    this.name,
    this.phone,
    this.email,
    this.password,
  );
}
