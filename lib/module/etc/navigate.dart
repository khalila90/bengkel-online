import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

GlobalKey<NavigatorState> navKey = GlobalKey<NavigatorState>();

class Navigate {
  static Future<void> push(Widget target, {String? routeName}) => navKey.currentState!.push(MaterialPageRoute(builder: (_) => target));

  static void pop() => navKey.currentState!.pop();

  static Future<void> pushReplace(Widget target, {String? routeName}) => navKey.currentState!.pushReplacement(MaterialPageRoute(builder: (_) => target));
}
