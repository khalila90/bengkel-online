import 'dart:ffi';

import 'package:bengkel_online/view/home/home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  const MyButton({
    Key? key,
    required this.text,
    this.size,
    this.color,
    required this.function,
  }) : super(key: key);

  final String text;
  final Size? size;
  final Color? color;
  final VoidCallback function;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          onPrimary: color,
          primary: color,
          fixedSize: size,
          padding: EdgeInsets.symmetric(horizontal: 20),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
        ),
        onPressed: function,
        child: Text(
          text,
          style: TextStyle(
            fontFamily: "Saira",
            fontSize: 25,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
