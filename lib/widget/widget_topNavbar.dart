import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyTopNavigationBar extends StatelessWidget {
  const MyTopNavigationBar({Key? key, this.title, this.trailing}) : super(key: key);

  final String? title;
  final String? trailing;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 20,
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 30),
      color: Color.fromRGBO(133, 157, 242, 1),
      child: ListTile(
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        title: Text(
          title!,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        trailing: Text(
          trailing!,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
