import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyTextField extends StatelessWidget {
  const MyTextField({
    Key? key,
    required this.hint,
    this.focusNode,
    this.isPassword,
    this.nextFocusNode,
    this.textEditingController,
    this.width = 0,
  }) : super(key: key);

  final String hint;
  final FocusNode? focusNode;
  final bool? isPassword;
  final FocusNode? nextFocusNode;
  final TextEditingController? textEditingController;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width == 0 ? MediaQuery.of(context).size.width * 0.85 : width,
      child: TextFormField(
        controller: textEditingController,
        obscureText: isPassword! ? true : false,
        focusNode: focusNode,
        maxLines: 1,
        cursorHeight: 15,
        style: TextStyle(
          fontSize: 20,
          height: 1,
          fontFamily: "Saira",
        ),
        decoration: InputDecoration(
          hintText: hint,
          hintStyle: TextStyle(
            color: Colors.grey,
            fontSize: 20,
            fontFamily: "Saira",
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(
              color: Colors.grey,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(
              color: Colors.grey,
            ),
          ),
        ),
        onFieldSubmitted: (v) {
          if (nextFocusNode != null) {
            FocusScope.of(context).requestFocus(nextFocusNode);
          }
        },
      ),
    );
  }
}
