import 'package:bengkel_online/view/auth/register.dart';
import 'package:bengkel_online/view/home/home.dart';
import 'package:bengkel_online/widget/widget_button.dart';
import 'package:bengkel_online/widget/widget_textField.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  LoginState createState() => LoginState();
}

class LoginState extends State<Login> {
  late FocusNode focUsername;
  late FocusNode focPassword;

  @override
  void initState() {
    super.initState();

    focUsername = FocusNode();
    focPassword = FocusNode();
  }

  @override
  void dispose() {
    focUsername.dispose();
    focPassword.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Container(
                  color: Color.fromRGBO(133, 157, 242, 1),
                  height: MediaQuery.of(context).size.height * 0.5,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image.asset(
                          "assets/image/engineer_1.png",
                          height: MediaQuery.of(context).size.height * 0.3,
                          fit: BoxFit.fitHeight,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          "Find Your Engineer",
                          style: TextStyle(
                            fontFamily: "Saira",
                            fontSize: 35,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Login Here",
                        style: TextStyle(
                          fontFamily: "Saira",
                          fontSize: 30,
                          color: Colors.black,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      MyTextField(
                        hint: "username",
                        focusNode: focUsername,
                        isPassword: false,
                        nextFocusNode: focPassword,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MyTextField(
                        hint: "password",
                        focusNode: focPassword,
                        isPassword: true,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      MyButton(
                        text: "Log In",
                        size: Size(150, 40),
                        color: Color.fromRGBO(88, 165, 237, 1),
                        function: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext context) => Home(),
                            ),
                          );
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextButton(
                        style: TextButton.styleFrom(
                          primary: Color.fromRGBO(73, 194, 210, 1),
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext context) => Register(),
                            ),
                          );
                        },
                        child: Text(
                          "Don't have account?",
                          style: TextStyle(
                            fontFamily: "Saira",
                            fontSize: 20,
                            color: Color.fromRGBO(73, 194, 210, 1),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
