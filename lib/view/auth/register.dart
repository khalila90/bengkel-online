import 'package:bengkel_online/models/user/register_models.dart';
import 'package:bengkel_online/widget/widget_button.dart';
import 'package:bengkel_online/widget/widget_textField.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  @override
  RegisterState createState() => RegisterState();
}

enum AccountType { engineer, customer, not }

class RegisterState extends State<Register> with SingleTickerProviderStateMixin {
  late FocusNode focName, focEmail, focPassword, focRePassword, focNoWa;
  TextEditingController contName = TextEditingController();
  TextEditingController contEmail = TextEditingController();
  TextEditingController contPassword = TextEditingController();
  TextEditingController contRePassword = TextEditingController();
  TextEditingController contPhone = TextEditingController();

  late TabController _controller;
  int _currentTabIndex = 0;

  AccountType? _accountType = AccountType.not;
  String? _selectedAccountType = 'not';

  @override
  void initState() {
    super.initState();

    focName = FocusNode();
    focEmail = FocusNode();
    focPassword = FocusNode();
    focRePassword = FocusNode();
    focNoWa = FocusNode();

    _controller = TabController(length: 2, vsync: this);
    _controller.addListener(_handleTabSelection);
  }

  @override
  void dispose() {
    focName.dispose();
    focEmail.dispose();
    focPassword.dispose();
    focRePassword.dispose();
    focNoWa.dispose();

    _controller.dispose();

    super.dispose();
  }

  _handleTabSelection() {
    setState(() {
      _currentTabIndex = _controller.index;
    });
    print(_currentTabIndex);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  color: Color.fromRGBO(133, 157, 242, 1),
                  height: MediaQuery.of(context).size.height * 0.5,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image.asset(
                          "assets/image/engineer_1.png",
                          height: MediaQuery.of(context).size.height * 0.3,
                          fit: BoxFit.fitHeight,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          "Find Your Engineer",
                          style: TextStyle(
                            fontFamily: "Saira",
                            fontSize: 35,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Select account type",
                        style: TextStyle(fontSize: 15),
                      ),
                      Container(
                        // height: 20,
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            ListTile(
                              title: const Text('Engineer'),
                              leading: Radio<AccountType>(
                                value: AccountType.engineer,
                                groupValue: _accountType,
                                onChanged: (AccountType? value) {
                                  setState(() {
                                    _accountType = value;
                                    _selectedAccountType = 'Engineer';
                                  });
                                },
                              ),
                            ),
                            ListTile(
                              title: const Text('Customer'),
                              leading: Radio<AccountType>(
                                value: AccountType.customer,
                                groupValue: _accountType,
                                onChanged: (AccountType? value) {
                                  setState(() {
                                    _accountType = value;
                                    _selectedAccountType = 'Customer';
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MyTextField(
                        textEditingController: contName,
                        hint: "Name",
                        focusNode: focName,
                        isPassword: false,
                        nextFocusNode: focEmail,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MyTextField(
                        textEditingController: contEmail,
                        hint: "E-mail",
                        focusNode: focEmail,
                        isPassword: false,
                        nextFocusNode: focNoWa,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MyTextField(
                        textEditingController: contPhone,
                        hint: "Phone",
                        focusNode: focNoWa,
                        isPassword: false,
                        nextFocusNode: focPassword,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MyTextField(
                        textEditingController: contPassword,
                        hint: "Password",
                        focusNode: focPassword,
                        isPassword: true,
                        nextFocusNode: focRePassword,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      MyTextField(
                        textEditingController: contRePassword,
                        hint: "Enter password again",
                        focusNode: focRePassword,
                        isPassword: true,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      MyButton(
                        text: "Sign Up",
                        size: Size(150, 40),
                        color: Color.fromRGBO(88, 165, 237, 1),
                        function: () {
                          print("Sign Up Pressed");
                          RegisterUserModel regUser = RegisterUserModel(
                            _selectedAccountType,
                            contName.value.text,
                            contPhone.value.text,
                            contEmail.value.text,
                            contPassword.value.text,
                          );
                          print(regUser.toString());
                        },
                      ),
                    ],
                  ),
                ),

                // Container(
                //   padding: EdgeInsets.only(
                //     bottom: MediaQuery.of(context).viewInsets.bottom,
                //   ),
                //   child: DefaultTabController(
                //     length: 2,
                //     child: Column(
                //       mainAxisSize: MainAxisSize.min,
                //       crossAxisAlignment: CrossAxisAlignment.start,
                //       children: [
                //         Container(
                //           width: MediaQuery.of(context).size.width * 0.5,
                //           child: TabBar(
                //             controller: _controller,
                //             labelStyle: TextStyle(
                //               fontFamily: "Saira",
                //               fontSize: 18,
                //               fontWeight: FontWeight.w100,
                //             ),
                //             indicatorColor: Colors.orange,
                //             indicatorWeight: 1,
                //             labelColor: Color.fromRGBO(18, 186, 223, 1),
                //             unselectedLabelColor: Colors.black,
                //             tabs: [
                //               Tab(
                //                 text: "Customer",
                //               ),
                //               Tab(
                //                 text: "Engineer",
                //               )
                //             ],
                //           ),
                //         ),
                //         Container(
                //           height: MediaQuery.of(context).size.height * 0.5,
                //           child: TabBarView(
                //             controller: _controller,
                //             children: [

                //               Container(
                //                 child: Column(
                //                   mainAxisAlignment: MainAxisAlignment.start,
                //                   crossAxisAlignment: CrossAxisAlignment.center,
                //                   children: [
                //                     SizedBox(
                //                       height: 15,
                //                     ),
                //                     MyTextField(
                //                       width: MediaQuery.of(context).size.width * 0.9,
                //                       hint: "Whatsapp(6289289938389)",
                //                       focusNode: focName,
                //                       isPassword: false,
                //                       nextFocusNode: focEmail,
                //                     ),
                //                     SizedBox(
                //                       height: 20,
                //                     ),
                //                     MyButton(
                //                       text: "Upload Form",
                //                       size: Size(MediaQuery.of(context).size.width * 0.8, 40),
                //                       color: Color.fromRGBO(27, 140, 175, 1),
                //                       function: () {
                //                         print("Upoad Form Pressed");
                //                       },
                //                     ),
                //                     SizedBox(
                //                       height: 15,
                //                     ),
                //                     Align(
                //                       alignment: Alignment.centerLeft,
                //                       child: Padding(
                //                         padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                //                         child: RichText(
                //                           text: TextSpan(
                //                             text: "Intruction: \n1. Download template from",
                //                             style: TextStyle(
                //                               color: Colors.black,
                //                             ),
                //                             children: [
                //                               TextSpan(
                //                                 text: " here",
                //                                 style: TextStyle(
                //                                   decoration: TextDecoration.underline,
                //                                   color: Colors.blue,
                //                                 ),
                //                               ),
                //                               TextSpan(
                //                                 text: "\n2. Print, fill, scan and upload your form here \n3. Admin will send confirmation to your whatsApp \n4. Follow the next step",
                //                                 style: TextStyle(
                //                                   color: Colors.black,
                //                                 ),
                //                               ),
                //                             ],
                //                           ),
                //                         ),
                //                       ),
                //                     ),
                //                     SizedBox(
                //                       height: 15,
                //                     ),
                //                     MyButton(
                //                       text: "Sign Up",
                //                       size: Size(150, 40),
                //                       color: Color.fromRGBO(88, 165, 237, 1),
                //                       function: () {
                //                         print("Sign Up Pressed");
                //                       },
                //                     ),
                //                     SizedBox(
                //                       height: 10,
                //                     ),
                //                   ],
                //                 ),
                //               )
                //             ],
                //           ),
                //         )

                //       ],
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
