import 'package:bengkel_online/models/menu_item_models.dart';
import 'package:bengkel_online/module/etc/navigate.dart';
import 'package:bengkel_online/view/home/chat.dart';
import 'package:bengkel_online/view/home/menu/repair.dart';
import 'package:bengkel_online/view/home/profile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  int _selectedNavbar = 0;
  List<MenuItem> menuItem = getItemMenu();

  void _changeSelectedNavbar(int index) {
    setState(() {
      _selectedNavbar = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              // Container(
              //   margin: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
              //   alignment: Alignment.center,
              //   padding: EdgeInsets.all(10),
              //   decoration: BoxDecoration(
              //     color: Colors.black,
              //     borderRadius: BorderRadius.all(
              //       Radius.circular(
              //         25,
              //       ),
              //     ),
              //   ),
              //   child: Text(
              //     "Welcome, User ",
              //     style: TextStyle(
              //       color: Color.fromRGBO(133, 157, 242, 1),
              //     ),
              //   ),
              // )
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25),
                ),
                elevation: 20,
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                color: Color.fromRGBO(133, 157, 242, 1),
                child: ListTile(
                  title: Text(
                    'Welcome, User',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  trailing: Text(
                    '30 Dec 2021',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),

              SizedBox(
                height: 10,
              ),
              Container(
                // color: Colors.black,
                margin: EdgeInsets.all(10),
                child: GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 100,
                    childAspectRatio: 1,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20,
                  ),
                  itemCount: menuItem.length,
                  itemBuilder: (BuildContext context, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => Repair(
                              title: menuItem[index].title,
                              code: menuItem[index].code,
                            ),
                          ),
                        );
                      },
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(menuItem[index].imgPath),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            menuItem[index].title,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                elevation: 20,
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                color: Colors.grey[300],
                child: Row(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      margin: EdgeInsets.only(left: 20),
                      child: Image.asset(
                        "assets/image/engineer_1.png",
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(29),
                      width: MediaQuery.of(context).size.width * 0.6,
                      child: Text(
                        'Bengkel online adalah sebuah platform untuk jasa home service atau jasa panggilan, \n\nHadir untuk anda mulai dari jasa perbaikan motor, mobil, sampai dengan PC/Laptop.',
                        style: TextStyle(
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                color: _selectedNavbar == 0 ? Colors.purple : Colors.grey,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.favorite_border,
                color: _selectedNavbar == 1 ? Colors.purple : Colors.grey,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => Chat(),
                    ),
                  );
                },
                child: Icon(
                  Icons.messenger_outline_outlined,
                  color: _selectedNavbar == 2 ? Colors.purple : Colors.grey,
                ),
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.settings,
                color: _selectedNavbar == 3 ? Colors.purple : Colors.grey,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: InkWell(
                child: Icon(
                  Icons.person_outline,
                  color: _selectedNavbar == 4 ? Colors.purple : Colors.grey,
                ),
                onTap: () {
                  Navigate.push(Profile());
                },
              ),
              label: '',
            ),
          ],
          currentIndex: 0,
          selectedItemColor: Colors.green,
          unselectedItemColor: Colors.grey,
          showUnselectedLabels: true,
          onTap: _changeSelectedNavbar,
        ),
      ),
    );
  }
}
