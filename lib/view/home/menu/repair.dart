import 'dart:math';

import 'package:bengkel_online/models/models_RepairItem.dart';
import 'package:bengkel_online/widget/widget_topNavbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Repair extends StatefulWidget {
  const Repair({Key? key, this.title, this.code}) : super(key: key);

  final String? title;
  final String? code;

  @override
  RepairState createState() => RepairState();
}

class RepairState extends State<Repair> {
  List<ModelRepairItem> modelRepair = getRepairModel();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              MyTopNavigationBar(
                title: widget.title!,
                trailing: "",
              ),
              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: Random().nextInt(30),
                itemBuilder: (BuildContext context, index) {
                  return Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    elevation: 10,
                    shadowColor: Colors.grey,
                    margin: EdgeInsets.only(
                      left: 20,
                      bottom: 30,
                    ),
                    color: Colors.grey[200],
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        vertical: 10,
                      ),
                      child: ListTile(
                        leading: Image.asset(
                          "assets/icons/default_user.png",
                        ),
                        title: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Nama : ${modelRepair[Random().nextInt(2)].name}",
                            ),
                            Text(
                              "Bidang Jasa : ${widget.title!}",
                            ),
                            Text(
                              "Nama : ${modelRepair[Random().nextInt(2)].age.toString()}",
                            ),
                          ],
                        ),
                        trailing: Text(
                          modelRepair[Random().nextInt(2)].distance,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
